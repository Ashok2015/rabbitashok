<?php

Class History_model extends CI_Model
{
  /**
   *  Function for saving the search in database
   *  @param array   $baseURI   Data to be saved
   *  @return number $insert_id Id stored in database
  */
  public function add($data)
  {
    $d = array();
    $d['location'] = $data['place'];
    if ($this->db->insert('history', $d)) {
      return $this->db->insert_id();
    } else {
      return FALSE;
    }
  }

  /**
   *  Function for saving the search in database
   *  @return array $result_array All the rows of history table in descending order 
  */
  public function get_history()
  {
    $this->db->select('*');
    $this->db->from('history');
    $this->db->order_by("id","desc");
    //$this->db->limit('5');
    $query=$this->db->get();
    if($query->num_rows()>0){
      return $query->result_array();
    }
  }
}
