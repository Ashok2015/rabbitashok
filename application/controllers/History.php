<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * This controller is used for the listing of search
	 */
	/**/
	public function index()
	{
		$data['results']=$this->History_model->get_history();
		$this->template->load('template','history/list',$data);
	}
}
