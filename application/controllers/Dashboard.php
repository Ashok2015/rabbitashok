<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://ashok.ecommercenepal.com/
	 *
	 */
	public function index()
	{	
		$this->template->load('template', 'dashboard');
	}
}
