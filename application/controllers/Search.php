<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	/**
	 *Creating an istance of Search Controller 
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->config('twitter');
	}
	
	/**
	 * Index Page for this controller.
	 *	This function gets the parameter from url and first search on its cache for data
	 *	If data found in cache, gets the result
	 *	Else query in the twitter api and fetch the data.
	 */
	public function index()
	{
		$this->History_model->add($this->input->get());
		$place = $_GET['place'];
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		$this->load->config('twitter');

		/*Caching time from config*/
		$tweetcachetime=$this->config->item('tweet_cache_time');
		$data=array();

		/*If data not found in Cache make api call and fetch data and store data in cache*/
		if(!$foo = $this->cache->get($place))
		{
			$twitter_data = $this->queryTwitter($place);
			$results =$twitter_data->statuses;
			$tweets=array();
			foreach ($results as $result) {
				$temparray = array();
				$user=$result->user;
				$geo = $result->geo;
				$id = $result->id;
				/*If coordinates create a new temp array*/
				if(isset($geo->coordinates)){
					$coordinates= $geo->coordinates;
					$temparray['created_at']= $result->created_at;
					$temparray['username'] = $user->name;
					$temparray['description'] = $user->description;
					$temparray['profile_image_url'] = $user->profile_image_url;
					$temparray['latitude'] = $coordinates[0];
					$temparray['longitude'] = $coordinates[1];
					$tweets[$id] = $temparray;
				}

			}
			$temp[$place]=$tweets;
			if(count($results)>1){
			    // Save data in cache for 1 hour
				$this->cache->save($place, $temp, $tweetcachetime);
			}
			$data['results']=$temp;
		}else{
		/*If data found in Cache fetch it from cache*/
			$data['results']=$foo;
		}$data['place']=$place;
     	//return $cache_data;
     	$this->template->load('template','search/search_result',$data);
	}

	/**
	 *	Function for building base string for query in twitter api
	 * 	@param string 	$baseURI 	API request uri
	 * 	@param string 	$method 	Method to be used GET or POST
	 * 	@param string 	$params 	Parameters to be arranged accordingly
	 *  @return string 
	*/
	function buildBaseString($baseURI, $method, $params) {
		$r = array();
		ksort($params);
		foreach($params as $key=>$value){
			$r[] = "$key=" . rawurlencode($value);
		}
		return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
	}


	/**
	 *	Function for building authorization header for query in twitter api
	 * 	@param string 	$oauth 	all the required auth variables for authentication
	 *  @return string 
	*/
	function buildAuthorizationHeader($oauth) {
		$r = 'Authorization: OAuth ';
		$values = array();
		foreach($oauth as $key=>$value)
			$values[] = "$key=\"" . rawurlencode($value) . "\"";
		$r .= implode(', ', $values);
		return $r;
	}

	/**
	 *	Function for building authorization header for query in twitter api
	 * 	@param string 	$search 	Location from which tweets to be fetch
	 *  @return object 
	*/
	function queryTwitter($search)
	{
		$url = "https://api.twitter.com/1.1/search/tweets.json";
		if($search != "")
		$search = "#".$search;
		$query = array('');
		$this->load->config('twitter');
		$tweet_radius = $this->config->item('oauth_token');
		/*Converting address into latitude and longitude*/
		$Address = urlencode($search);
		$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true";
		$xml = simplexml_load_file($request_url) or die("url not loading");
	    $status = $xml->status;
	    if($search != "")
			$search = "#".$search;
	    if ($status=="OK") {
	        $Lat = $xml->result->geometry->location->lat;
	        $Lon = $xml->result->geometry->location->lng;
	       	$query = array( 'count' => 100, 'q' => 'geocode='.$Lat.','.$Lon.','.$tweet_radius, "result_type" => "recent");
	    }else{
	    	$query = array( 'count' => 100, 'q' => urlencode($search), "result_type" => "recent");

	    }

		$oauth_access_token = $this->config->item('oauth_token');
		$oauth_access_token_secret = $this->config->item('oauth_token_secret');
		$consumer_key = $this->config->item('consumer_key');
		$consumer_secret = $this->config->item('consumer_secret');

		$oauth = array(
			'oauth_consumer_key' => $consumer_key,
			'oauth_nonce' => time(),
			'oauth_signature_method' => 'HMAC-SHA1',
			'oauth_token' => $oauth_access_token,
			'oauth_timestamp' => time(),
			'oauth_version' => '1.0');

		$base_params = empty($query) ? $oauth : array_merge($query,$oauth);
		$base_info = $this->buildBaseString($url, 'GET', $base_params);
		$url = empty($query) ? $url : $url . "?" . http_build_query($query);

		$composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
		$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
		$oauth['oauth_signature'] = $oauth_signature;

		$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
		$options = array( CURLOPT_HTTPHEADER => $header,
			CURLOPT_HEADER => false,
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false);

		$feed = curl_init();
		curl_setopt_array($feed, $options);
		$json = curl_exec($feed);
		curl_close($feed);
		$twitter_data = json_decode($json);
		return $twitter_data;
	}


}
