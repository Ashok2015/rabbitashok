<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tweet Map</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url()?>/assets/css/script.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="jumbotron text-center">
	    	<h1>Tweet Map</h1>
	    </div>
			
		<?php echo $contents; ?>

		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTQoN4uyXJX-5WMydCyzdH7VD5Jw3lAmo&callback=initMap&libraries=places" ></script>
	</body>
</html>
