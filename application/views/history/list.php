<div class="container">
  <div class="tweet-map-container">
    <div class="row">
      <div class="col-sm-12">
        <h3>History Page</h3>
        <div class="back"><a href="<?php echo base_url()?>">< Back to Tweets!</a></div>
        <div class="table-responsive">
          <div class="table-container">
            <table class="table table-bordered">                 
              <tbody> 
                <?php if(count($results)>0):
                  foreach($results as $result):?>
                  <tr>
                    <td scope="row"><a href="<?php echo base_url()?>search?place=<?php echo $result['location']?>"><?php echo $result['location']; ?></td> 
                    </tr> 
                  <?php endforeach;
                else:?>
                  <tr> 
                    <th scope="row">No History Available</th> 
                  </tr>
                <?php endif;?> 
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>