<div class="container">
	<div class="tweet-map-container">
		<div class="row">
			<div class="col-sm-12 tweet-title">
				<h2></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div id="tweet-map"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="search-block">
					<form action="<?php echo base_url();?>search" method="get">
						<div class="col-xs-12 col-sm-6 search-input">
							<div class="form-group">
								<input name="place" type="text" id="address-field" placeholder="City name" required="required">
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 buttons search-btn">
							<button type="submit" id="search-btn">Search</button>
						</div>
						<div class="col-xs-6 col-sm-3 buttons history-btn">
							<a href="<?php echo base_url();?>history">History</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function initMap() {
		var map = new google.maps.Map(document.getElementById('tweet-map'), {
			zoom: 11,
			center: {lat: -34.397, lng: 150.644},
			mapTypeId: 'roadmap',
			styles: [
                {
                	"featureType": "all",
                	"elementType": "all",
                	"stylers": [
                	{
                		"saturation": -100
                	},
                	{
                		"gamma": 0.5
                	}
                	]
                }
                ]
		});
	}
</script>
