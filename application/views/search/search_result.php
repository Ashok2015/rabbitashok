<?php 
$tweets=json_encode($results[$place]);
?>
<div class="container">
	<div class="tweet-map-container">
		<div class="row">
			<div class="col-sm-12 tweet-title">
				<h2>Tweets About <?php echo $place; ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div id="tweet-map"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="search-block">
					<form action="<?php echo base_url();?>search" method="get">
						<div class="col-xs-12 col-sm-6 search-input">
							<div class="form-group">
								<input name="place" type="text" id="address-field" value="<?php echo $place ?>" placeholder="City name" required="required">
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 buttons search-btn">
							<button type="submit" id="search-btn">Search</button>
						</div>
						<div class="col-xs-6 col-sm-3 buttons history-btn">
							<a href="<?php echo base_url();?>history">History</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var myJsonString_tweets = JSON.parse(JSON.stringify(<?php echo $tweets ?>));
	function initMap() {
		var mapDiv = document.getElementById('tweet-map');
		var map;
		var address = "<?php echo $place; ?>";
		var geocoder = new google.maps.Geocoder();
		    // Get LatLng information by name
		    geocoder.geocode({
		    	"address": address
		    }, function(results, status){
		    	map = new google.maps.Map(mapDiv, {
		                // Center map (but check status of geocoder)
		                center: results[0].geometry.location,
		                zoom: 11,
		                mapTypeId: google.maps.MapTypeId.ROADMAP,
		                styles: [
		                {
		                	"featureType": "all",
		                	"elementType": "all",
		                	"stylers": [
		                	{
		                		"saturation": -100
		                	},
		                	{
		                		"gamma": 0.5
		                	}
		                	]
		                }
		                ]
		            });
		    	$.each( myJsonString_tweets, function( key, tweet_value ) {
		    		var marker = new google.maps.Marker({
		    			position: new google.maps.LatLng(tweet_value.latitude, tweet_value.longitude),
		    			map: map,
		    			icon: tweet_value.profile_image_url,
		    			title: tweet_value.username,
		    		});

		    		var infoWindow = new google.maps.InfoWindow();

		    		google.maps.event.addListener(marker, 'click', function () {
		    			var markerContent = 'Tweet : ' + tweet_value.description + '<br>' + 'When : '+tweet_value.created_at;
		    			infoWindow.setContent(markerContent);
		    			infoWindow.open(map, this);
		    		});
		    	});
		    });
		}
	</script>
